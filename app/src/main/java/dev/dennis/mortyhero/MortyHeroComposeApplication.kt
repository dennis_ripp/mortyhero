package dev.dennis.mortyhero

import android.app.Application
import com.surrus.common.di.initKoin
import dev.dennis.mortyhero.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.logger.Level

class MortyHeroComposeApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@MortyHeroComposeApplication)
            modules(appModule)
        }
    }

}