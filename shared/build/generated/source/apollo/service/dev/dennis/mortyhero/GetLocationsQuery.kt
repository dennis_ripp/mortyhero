//
// AUTO-GENERATED FILE. DO NOT MODIFY.
//
// This class was automatically generated by Apollo GraphQL version '3.3.0'.
//
package dev.dennis.mortyhero

import com.apollographql.apollo3.api.Adapter
import com.apollographql.apollo3.api.CompiledField
import com.apollographql.apollo3.api.CustomScalarAdapters
import com.apollographql.apollo3.api.Query
import com.apollographql.apollo3.api.json.JsonWriter
import com.apollographql.apollo3.api.obj
import dev.dennis.mortyhero.adapter.GetLocationsQuery_ResponseAdapter
import dev.dennis.mortyhero.adapter.GetLocationsQuery_VariablesAdapter
import dev.dennis.mortyhero.fragment.LocationDetail
import dev.dennis.mortyhero.selections.GetLocationsQuerySelections
import kotlin.Int
import kotlin.String
import kotlin.Unit
import kotlin.collections.List

public data class GetLocationsQuery(
  public val page: Int?,
) : Query<GetLocationsQuery.Data> {
  public override fun id(): String = OPERATION_ID

  public override fun document(): String = OPERATION_DOCUMENT

  public override fun name(): String = OPERATION_NAME

  public override fun serializeVariables(writer: JsonWriter,
      customScalarAdapters: CustomScalarAdapters): Unit {
    GetLocationsQuery_VariablesAdapter.toJson(writer, customScalarAdapters, this)
  }

  public override fun adapter(): Adapter<Data> = GetLocationsQuery_ResponseAdapter.Data.obj()

  public override fun rootField(): CompiledField = CompiledField.Builder(
    name = "data",
    type = dev.dennis.mortyhero.type.Query.type
  )
  .selections(selections = GetLocationsQuerySelections.root)
  .build()

  public data class Data(
    /**
     * 
     * Get the list of all locations
     */
    public val locations: Locations,
  ) : Query.Data

  public data class Locations(
    public val info: Info,
    public val results: List<Result?>,
  ) {
    public fun resultsFilterNotNull(): List<Result> = results.filterNotNull()
  }

  public data class Info(
    /**
     * 
     * The length of the response.
     */
    public val count: Int,
    /**
     * 
     * The amount of pages.
     */
    public val pages: Int,
    /**
     * 
     * Number of the next page (if it exists)
     */
    public val next: Int?,
  )

  public data class Result(
    public val __typename: String,
    /**
     * Synthetic field for 'LocationDetail'
     */
    public val locationDetail: LocationDetail,
  )

  public companion object {
    public const val OPERATION_ID: String =
        "04121a9722ec70781ddfc7ba05c019530662ad8bce24a0a8e624379ece575881"

    /**
     * The minimized GraphQL document being sent to the server to save a few bytes.
     * The un-minimized version is:
     *
     * query GetLocations($page: Int) {
     *   locations(page: $page) {
     *     info {
     *       count
     *       pages
     *       next
     *     }
     *     results {
     *       __typename
     *       ...LocationDetail
     *     }
     *   }
     * }
     *
     * fragment LocationDetail on Location {
     *   id
     *   name
     *   type
     *   dimension
     *   residents {
     *     id
     *     name
     *     image
     *   }
     * }
     */
    public const val OPERATION_DOCUMENT: String =
        "query GetLocations(${'$'}page: Int) { locations(page: ${'$'}page) { info { count pages next } results { __typename ...LocationDetail } } }  fragment LocationDetail on Location { id name type dimension residents { id name image } }"

    public const val OPERATION_NAME: String = "GetLocations"
  }
}
