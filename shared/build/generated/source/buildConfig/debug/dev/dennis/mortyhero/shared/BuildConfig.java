/**
 * Automatically generated file. DO NOT MODIFY
 */
package dev.dennis.mortyhero.shared;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "dev.dennis.mortyhero.shared";
  public static final String BUILD_TYPE = "debug";
}
