pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }

}
rootProject.name = "MortyHero"


include(":app")
include(":shared")
